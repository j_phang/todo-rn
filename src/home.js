import React, { Component } from 'react'
import { Text, ToastAndroid, Alert, BackHandler, ImageBackground, ScrollView, View, Dimensions } from 'react-native'
import { Container, Item, Button, Header, Body, Title, Left, Right, Icon, Content, Card, CardItem, Input, Label, Picker, List, ListItem } from 'native-base';
import { withNavigation } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import { FloatingAction } from "react-native-floating-action";
import Modal from 'react-native-modal';
import styles from './style';

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: [],
            modal: false,
            title: '',
            body: '',
            dueDate: '',
            status: false
        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.showAlert1)
        this.fetch()
        setInterval(this.fetch, 1000)
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.showAlert1);
    }

    async fetch() {
        const token = await AsyncStorage.getItem('@token')
        try {
            const getPost = async () => await Axios.get(
                //http://basic-todoapi.herokuapp.com/api/user/todo/show
                //http://basic-todoapi.herokuapp.com/api/user/todo/create
                `http://basic-todoapi.herokuapp.com/api/user/todo/show`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            getPost()
                .then(result => {
                    this.setState({
                        data: result.data.result.todo
                    })
                    console.log(result.data.result.todo)
                })
                .catch(e => {
                    console.log(e)
                })
        }
        catch (e) {
            console.log(e)
        }
    }

    async deleteTodo(id) {
        try {
            const token = await AsyncStorage.getItem('@token')
            const delPosted = async (ids) => await Axios.delete(
                `http://basic-todoapi.herokuapp.com/api/user/todo/delete/${ids}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )
            delPosted(id)
                .then(result => {
                    ToastAndroid.show(`Data telah didelete`, ToastAndroid.SHORT)
                    this.fetch()
                    console.log(result)
                })
                .catch(e => {
                    console.log(e)
                })
        }
        catch (e) {
            console.log(e)
        }
    }

    async AddData() {
        const token = await AsyncStorage.getItem('@token')
        try {
            const postData = async objParam => await Axios.post(
                // https://app-todoapps.herokuapp.com/todos
                `http://basic-todoapi.herokuapp.com/api/user/todo/create`, objParam, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            postData({
                title: this.state.title,
                status: false,
                dueDate: this.state.dueDate,
                body: this.state.body
            })
                .then(response => {
                    console.log(response)
                    this.fetch()
                    this.setState({
                        title: '',
                        body: '',
                        dueDate: '',
                    })
                    ToastAndroid.show('Data berhasil ditambahkan', ToastAndroid.SHORT)
                })
                .catch(e => {
                    ToastAndroid.show('Anda belum bisa nambah data', ToastAndroid.SHORT)
                    console.log(e)
                })
        }
        catch (e) {
            ToastAndroid.show('Gagal API', ToastAndroid.SHORT)
            console.log(e)
        }
    }

    async updateData(id) {
        const token = await AsyncStorage.getItem('@token')
        try {
            const updateData = async (ids, params) => await Axios.put(
                //http://basic-todoapi.herokuapp.com/api/user/todo/update/
                //https://app-todoapps.herokuapp.com/todos/
                `http://basic-todoapi.herokuapp.com/api/user/todo/update/${ids}`, params, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `jwt ${token}`
                    }
                }
            )

            updateData(id, {
                status: this.state.status
            })
                .then(response => {
                    ToastAndroid.show(`Status berubah menjadi ${this.state.status}`, ToastAndroid.SHORT)
                    this.fetch()
                })
                .catch(e => {
                    ToastAndroid.show('WAHAHAH GAGAL ANJIR', ToastAndroid.SHORT)
                    console.log(e)
                })
        }
        catch (e) {
            console.log(e)
        }
    }

    toggleModal = () => {
        this.setState({
            modal: !this.state.modal
        })
    }

    logout = () => {
        AsyncStorage.setItem('@token', '')
        this.props.navigation.pop()
    }

    showAlert1 = () => {
        Alert.alert(
            'Logout',
            `Apakah anda yakin ingin Logout ?`,
            [
                { text: 'Cancel', onPress: () => console.log('Cancel') },
                { text: 'OK', onPress: () => this.props.navigation.pop() }

            ]
        );
    }

    showAlertDelete = (wow) => {
        Alert.alert(
            'Delete Data',
            `Apakah anda yakin ingin Hapus?`,
            [
                { text: 'Cancel', onPress: () => console.log('Cancel') },
                { text: 'OK', onPress: () => this.deleteTodo(wow) }

            ]
        );
    }
    render() {
        const loops = this.state.data.map(value => {
            return (
                <Card
                    key={value._id}
                    style={
                        {
                            borderColor: value.status ? 'rgba(127, 255, 0,  1)' : 'rgba(127, 255, 0,  0)',
                            borderWidth: 3,
                            backgroundColor: 'rgba(0,0,0,0)'
                        }
                    }
                >
                    <Left style={{ marginLeft: '-60%' }}>
                        <CardItem header style={{ backgroundColor: 'rgba(0,0,0,0)' }}>
                            <Text style={{ color: 'white' }}>
                                {value.title}
                            </Text>
                        </CardItem>
                        <CardItem style={{ backgroundColor: 'rgba(0,0,0,0)' }}>
                            <Text style={{ color: 'white' }}>
                                Berakhir : {value.dueDate}
                            </Text>
                        </CardItem>
                        <CardItem style={{ backgroundColor: 'rgba(0,0,0,0)' }}>
                            <Text style={{ color: 'white' }}>
                                {value.body}
                            </Text>
                        </CardItem>
                    </Left>
                    <Right style={styles.right}>
                        <Icon
                            style={{
                                margin: '5%',
                                color: 'white'
                            }}
                            type="FontAwesome5"
                            name="trash"
                            onPress={
                                () => this.showAlertDelete(value._id)
                            }
                        />
                        <Icon
                            style={{
                                margin: '5%',
                                color: 'white'
                            }}
                            type="FontAwesome5"
                            name="pen"
                            onPress={
                                () => {
                                    this.setState({
                                        status: !value.status
                                    })
                                    this.updateData(value._id)
                                }
                            }
                        />
                    </Right>
                </Card>
            )
        })
        return (
            <Container>
                <ImageBackground
                    source={require('./img/Home_2.jpg')}
                    style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
                >
                    <Header transparent>
                        <Left>
                            <Icon
                                type="SimpleLineIcons"
                                name="logout"
                                onPress={
                                    () => this.showAlert1()
                                }
                                style={styles.icons}
                            />
                        </Left>
                        <Body>
                            <Title style={{ marginLeft: '15%' }}>Home</Title>
                        </Body>
                    </Header>
                    <ScrollView keyboardShouldPersistTaps='always'>
                        <View>
                            {loops}
                        </View>
                    </ScrollView>
                    <Modal
                        isVisible={this.state.modal}
                        onBackButtonPress={
                            () => {
                                this.toggleModal()
                            }
                        }
                        onBackdropPress={
                            () => {
                                this.toggleModal()
                            }
                        }
                        style={{
                            display: 'flex',
                            width: '100%',
                            height: '100%',
                            justifyContent: 'center',
                            alignContent: 'center'
                        }}
                    >
                        <Content
                            style={
                                {
                                    backgroundColor: 'transparent',
                                    width: '100%',
                                    height: '100%',
                                    marginTop: '50%'
                                }
                            }
                        >
                            <Item style={{
                                display: "flex",
                                marginLeft: '15%',
                                width: `60%`,
                                justifyContent: 'center'
                            }} floatingLabel>
                                <Label>
                                    <Text style={{ color: 'white' }}>Title</Text>
                                </Label>
                                <Input style={{ color: 'white' }} keyboardType='default' value={this.state.title} onChangeText={(text) => this.setState({ title: text })} />
                            </Item>
                            <Item style={{
                                display: "flex",
                                marginLeft: '15%',
                                width: `60%`,
                                justifyContent: 'center'
                            }} floatingLabel>
                                <Label>
                                    <Text style={{ color: 'white' }}>Body</Text>
                                </Label>
                                <Input style={{ color: 'white' }} keyboardType='default' value={this.state.body} onChangeText={(text) => this.setState({ body: text })} />
                            </Item>
                            <Item style={{
                                display: "flex",
                                marginLeft: '15%',
                                width: `60%`,
                                justifyContent: 'center'
                            }} floatingLabel>
                                <Label>
                                    <Text style={{ color: 'white' }}>DueDate</Text>
                                </Label>
                                <Input style={{ color: 'white' }} keyboardType='default' value={this.state.dueDate} onChangeText={(text) => this.setState({ dueDate: text })} />
                            </Item>
                            <Item
                                style={{
                                    display: "flex",
                                    borderColor: 'transparent',
                                    marginLeft: '-10%',
                                    justifyContent: "center"
                                }}
                            >
                                <Button
                                    style={{
                                        width: '60%'
                                    }}
                                    full
                                    transparent
                                    title="Hide modal"
                                    onPress={
                                        () => {
                                            this.AddData()
                                            this.toggleModal()
                                        }
                                    }
                                >
                                    <Text style={styles.texts}>Submit</Text>
                                </Button>
                            </Item>
                            <Item
                                style={styles.button}
                            >
                                <Button
                                    full
                                    style={{
                                        display: "flex",
                                        borderColor: 'transparent',
                                        marginLeft: '-10%',
                                        justifyContent: "center"
                                    }}
                                    transparent
                                    onPress={
                                        () => {
                                            this.toggleModal()
                                        }
                                    }
                                >
                                    <Text style={styles.texts}>Cancel</Text>
                                </Button>
                            </Item>
                        </Content>
                    </Modal>
                    <FloatingAction
                        onPressMain={() => this.toggleModal()}
                        showBackground={false}
                        position='center'
                    />
                </ImageBackground>
            </Container>
        )
    }
}
export default withNavigation(Home)