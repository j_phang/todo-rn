import React, { Component } from 'react'
import { Text, Dimensions, Alert, ToastAndroid, ImageBackground, KeyboardAvoidingView } from 'react-native'
import { ScrollView } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import { Item, View, Container, Header, Body, Title, Label, Input, Button, Picker } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import Axios from 'axios';
import styles from './style';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            language: ''
        }
    }
    async login() {
        try {
            const postData = async (objParam) => await Axios.post(
                //https://abc-todo.herokuapp.com/api/user/login Zul-no Link
                //https://app-todoapps.herokuapp.com/users/login Fifa de Link
                //http://basic-todoapi.herokuapp.com/api/user/login Joe punya Link
                `http://basic-todoapi.herokuapp.com/api/user/login`, objParam, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
            postData({
                username: this.state.username,
                password: this.state.password
            })
                .then(response => {
                    ToastAndroid.show(response.data.result, ToastAndroid.SHORT)
                    AsyncStorage.setItem('@token', response.data.result)
                    ToastAndroid.show('Selamat Anda Dapat melakukan Login', ToastAndroid.SHORT)
                    console.log(`Bisa Log In Sayang :*`)
                    this.setState({
                        username: '',
                        password: ''
                    })
                    this.props.navigation.navigate('Homed')
                })
                .catch(e => {
                    this.showAlert1(e)
                })
        }
        catch (e) {
            this.showAlert1(e)
        }
    }
    showAlert1 = (wow) => {
        Alert.alert(
            'Log In',
            `${wow}`,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ]
        );
    }
    render() {
        return (
            <Container>
                <ImageBackground
                    source={require('./img/Login.jpg')}
                    style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
                >
                    <Header transparent>
                        <Body>
                            <Title style={{ marginLeft: '25%' }}>Login Page Sayang</Title>
                        </Body>
                    </Header>
                    <KeyboardAwareScrollView>
                        <View
                            style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignContent: 'center',
                                marginTop: '80%'
                            }}
                        >
                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        username Sayang
                                </Text>
                                </Label>
                                <Input
                                    style={{ color: 'white' }}
                                    keyboardType="default"
                                    value={this.state.username}
                                    onChangeText={
                                        (text) => this.setState({ username: text })
                                    }
                                    keyboard
                                />
                            </Item>
                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>
                                        Password
                                </Text>
                                </Label>
                                <Input
                                    style={{ color: 'white' }}
                                    keyboardType="default"
                                    secureTextEntry={true}
                                    value={this.state.password}
                                    onChangeText={
                                        (text) => this.setState({ password: text })
                                    }
                                />
                            </Item>
                            <Item style={styles.button}>
                                <Button rounded bordered success style={styles.buttons} full onPress={() => this.login()}>
                                    <Text style={styles.text}>Login</Text>
                                </Button>
                            </Item>
                            <Item style={styles.button}>
                                <Button rounded bordered success style={styles.buttons} full onPress={() => this.props.navigation.navigate('Register')}>
                                    <Text style={styles.text}>Register</Text>
                                </Button>
                            </Item>
                        </View>
                        </KeyboardAwareScrollView>
                </ImageBackground>
            </Container >
        )
    }
}
export default withNavigation(Login)