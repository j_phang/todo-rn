import React, { Component } from 'react'
import Home from './src/home.js';
import Register from './src/register.js';
import Login from './src/login.js';
import { createStackNavigator, createAppContainer } from 'react-navigation';

class HomeScreen extends Component{
  render(){
    return <Login />
  }
}

class Registers extends Component{
  render(){
    return <Register />
  }
}

class Homes extends Component{
  render(){
    return <Home />
  }
}

const StackNav = createStackNavigator({
  Logins: {
    screen: HomeScreen
  },
  Register: {
    screen: Registers
  },
  Homed: {
    screen: Homes
  }
},
  {
    initialRouteName: 'Logins',
    headerMode: 'none'
  })

const AppContainer = createAppContainer(StackNav)

export default class App extends Component {
  render() {
    return <AppContainer />
  }
}
